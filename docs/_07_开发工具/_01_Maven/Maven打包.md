# Maven打包

可以使用Maven的 `mvn clean package` 指令 或 `mvn clean package -Dmaven.test.skip=true`指令进行打包，后者表示不执行test中的测试函数。

然而，直接打包的结果可以用于作为其他工程的依赖，但并不能通过`java -jar xxx.jar`执行，会报 xxx.jar中没有主清单属性 的错误。jar包中没有此文件 jdk并不能知道哪里才是程序的入口。

> 直接打出的war包是没有问题的？

> 如何打出可执行的 jar 包

在Maven中有多种插件可以打出可执行jar包，我们只需要在pom文件中引入合适的插件即可。

1. 使用maven-jar-plugin和maven-dependency-plugin插件打包
2. 使用maven-assembly-plugin插件打包
3. 使用maven-shade-plugin插件打包

这里只记录 maven-shade-plugin插件打包 ，更过内容：[Maven3种打包方式](https://www.cnblogs.com/sfnz/p/14207890.html)

```xml
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-shade-plugin</artifactId>
   <version>2.4.1</version>
   <executions>
      <execution>
         <phase>package</phase>
         <goals>
            <goal>shade</goal>
         </goals>
         <configuration>
            <transformers>
               <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                   <!--指定程序入口-->
                  <mainClass>qy.proj.main.App</mainClass>
               </transformer>
            </transformers>
         </configuration>
      </execution>
   </executions>
</plugin>
```

目前的需求只是打出可执行jar包，其他问题遇到了再说吧！[Maven3种打包方式](https://www.cnblogs.com/sfnz/p/14207890.html)

::: warning 注意
使用插件后，执行install，会将包含依赖的jar安装到maven仓库中，这时候再排除依赖就不生效了。
:::





















