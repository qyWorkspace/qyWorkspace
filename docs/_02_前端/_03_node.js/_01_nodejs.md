# NodeJS简介

## 1.1 什么是NodeJS？

​		NodeJS基于Google的V8引擎，可以进行JavaScript的解释执行，使得js代码脱离浏览器执行成为现实。NodeJs主要用于JS进行后端开发。

​		下载安装：http://nodejs.cn/download/

## 1.2 基本命令

-   node -v     查看nodejs的版本
-   node xxx.js    执行JS脚本

## 1.3 服务器应用开发

```javascript
const http = require("http"); // node中自带的require引入方法，http也是node中自带的服务对象
http.createServer( function(request,response){
// 发出http请求的头部信息
// http的状态码：200；OK
// 请求的内容类型：text/plain
response.writeHead(200,{"Content-Type": "text/plain"});
//响应的数据 "hello,welcome！" ,此时，并不支持中文（以后一定会解决！）
response.end("hello,welcome!");
} ).listen(8888); // 监听端口
console.log("服务器已启动，请访问 http://127.0.0.1:8888");
```

# 2. NPM

## 2.1 什么是NPM

​		npm 【Node Package Manager】 node包管理器，作用类似于Maven，用于管理第三方的 JS 文件。

​		安装nodejs会自带npm工具，安装在 nodejs安装目录下的 node_modules 文件夹中。

## 2.2 使用npm管理项目

1. 初始化项目：  npm init

    安装过程会要求输入一些项目相关信息，使用 npm init -y  可以 使用默认信息构建。

    初始化后会在项目中生成 package.json文件 ，存放项目的配置信息，

    ```json
    //  package.json
    {
      "name": "nodejs",  // 本项目的 名称
      "version": "1.0.0",  // 本项目的 版本
      "description": "",
      "main": "index.js",
      "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
      },
      "author": "mochen",
      "license": "ISC",
      "dependencies": {  // 本项目 的依赖
        "jquery": "^1.9.1"
      }
    }
    ```

## 2.3 修改npm镜像 和 存储地址

```cmd
# 经过下面的配置，以后所有的 npm install 都会经过淘宝的镜像地址下载
npm config set registry https://registry.npm.taobao.org
# 联网情况下也可以通过安装cnpm实现
npm install -g cnpm --registry=https://registry.npm.taobao.org


# 设置npm下载包时保存在本地的地址（建议英文目录）
npm config set prefix "F:\repository\04_NMPRegistry\npm_modules"
npm config set cache "F:\repository\04_NMPRegistry\npm_cache"
# 将prefix指定的路径配置到系统环境变量path中
...


# 查看npm配置信息
npm config list
```

## 2.4 安装依赖包

```cmd
npm install jquery@1.9.1    # 包名@版本号，不写@版本号 默认下载最新版
```

为项目安装所需要的的依赖。

安装之后会在项目中生成node_modules文件夹 和 package-lock.json 文件，前者存放下载的 Js 文件，后者存放包的版本等信息。

 ![image-20210101135956709](img/_01_nodejs/image-20210101135956709.png)

```json
// package-lock.json
{
  "name": "nodejs",
  "version": "1.0.0",
  "lockfileVersion": 1,
  "requires": true,
  "dependencies": {
    "jquery": {
		
      "version": "1.9.1",
      "resolved": "https://registry.npm.taobao.org/jquery/download/jquery-1.9.1.tgz",
      "integrity": "sha1-5M1INfqu+63lNYV2E8D8P/KtrzQ="
    }
  }
}
```







export xxx

export xxx



export default { ... }





import xxx from 'ppppp'

const ddd = require('xxxxx');
