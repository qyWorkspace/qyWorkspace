# CSS 基础

## 1. 引入方式

1. **内联样式表**：在要修上的元素上，通过 style 属性，指定样式。

2. **嵌入样式表**：通过 \<style\> . . . \</style\>，标签指定样式。

3. **引入外部样式表**：将样式定义在一个CSS文件中，在HTML中的 \<style\> 标签内，通过  @import url("CSS文件路径");  引入。

    ```html
    <!-- 如：-->
    <style>
    	@import url("css/test01.css");
    </style>
    ```

4. **连接到外部样式表**：通过 \<link\>标签连接到外部样式

    ```html
    <link rel="stylesheet" type="text/css" href="css/test01.css" />
    ```

>   关于外部导入css使用与@import的区别? 

1.  加载顺序不同 @import方式导入会先加载html，然后才导入css样式，那么如果网络条件不好，就会先看到没有 修饰的页面，然后才看到修饰后的页面。 如果使用link方式，它会先加载样式表，也就是说，我们看到的直接就是修饰的页面； 
2.  @import方式导入css样式，它是不支持javascript的动态修改的。而link支持。 

>   三种样式表的优先级：

满足**就近原则** 内联 > 嵌入 > 外部

## 2. css选择器

1. 元素选择器：直接写元素名

2. 类选择器：点 加 类型

3. id选择器：# 加 id名

4. 选择器组：

    -   两个或多个选择器，用**逗号**隔开，表示对这些选择器应用相同的样式。  如：```div, P```
    -   两个选择器连在一起写，表示选择同时具备这两个选择器特征的的元素。 如：```#ix01.class02```

5. 派生选择器：两个或多个选择器，用 **空格 或 >** 隔开

    用空格隔开，表示后代选择器；\> 隔开,表示子代选择器，\+ 隔开表示兄弟选择器    ~ 表示兄弟们选择器

    ```css
    div p{
        /* 表示选择div下的所有p标签 */
    }
    div>p{
        /* 只选择子标签中的p，孙子辈儿 及之后的 就不选了 */ 
    }
    div+p{
        /* 紧跟在被选div之后的一个兄弟p元素，
        p要在div之后
        注意div与p之间不能隔其他元素，
        只选择一个p
        */
    }
    div~p{
        /* 紧跟在被选div之后的所有兄弟p元素，
        p要在div之后
        所有 兄弟p都会被选中 ， 元素也不用紧挨着
        */
    }
    ```


## 3. 伪类选择器

**1. \<a\> 标签的伪类选择器：**

1. ```:link``` 指定连接未被点击前的样式

2. ```:visited``` 指定连接被访问后的样式

3. ```:hover``` 指定鼠标悬停时的样式

4. ```:active``` 指定连接被激活时（也就是被鼠标点住时）的样式

    注意：必须遵照上述顺序，如：visited如果出现在link之前，那么visited就会失效。

**2. 表单中的选择器**

```:focus``` 指定元素获得焦点时的样式。

**3. 通用伪类选择器：**

- ```:hover``` 指定鼠标悬停时的样式

- ```:first-child``` 被选元素如果有多个并列，则只选第一个

    如：下例中，111、aaa、AAA都会被选中。

    ```html
    <ol>
        <li>111</li>
        <li>222</li>
        <li>333</li>
        <ul>
            <li>aaa</li>
            <li>bbb</li>
            <li>ccc</li>
        </ul>
    </ol>
    <ul>
        <li>AAA</li>
        <li>BBB</li>
        <li>CCC</li>
    </ul>
    <style>
        li:first-child{
            color: red;
        }
    </style>
    ```

- ```:nth-child(n)``` 规则同上，但是是选择第 n 个.

    n的取值：

    -   数字，就是要选的元素索引，从 1 开始
    -   odd 匹配索引为**奇数**的，even 匹配索引为偶数的
    -   表达式，（an+b）如：3n+0  表示匹配索引为 3 的倍数的。

- ```:before```  在被选择的元素之前插入内容。

    ```css
    p:before
    { 
        /* 指定插入的内容 */
        content:"台词：-";
        /* 指定插入内容的样式 */ 
        background-color:yellow;
        color:red;
        font-weight:bold;
    }
    ```

- ```:after``` 同上，在被选元素之后插入。

## 4. 属性选择器

```[attribute]``` 选择具有指定属性的元素

```[attribute=value]``` 选择指定属性为value的元素

```[attribute*=value]``` 选择属性**包含**value的元素

```[attribute^=value]``` 选择属性以value开头的元素

```[attribute$=value]``` 选择属性以value结尾的元素

css属性选择器，好像不支持 != 的语法。





## 5. 文本属性

指定字体：font-family : value; 

字体大小：font-size : value;      px：像素   em：倍数 

字体加粗：font-weight : normal/bold; 

文本颜色：color : value; 

文本排列：text-align : left/right/center; 

文字修饰：text-decoration : none/underline; 

行高：line-height : value; 

首行文本缩进：text-indent : value （2em）;

## 6. 背景属性

- background-color 设置元素的背景颜色。 

- background-image 把图像设置为背景。 url(" . . . ")

- background-repeat 设置背景图像的墙纸效果，是否及如何重复 

    repeat：在垂直方向和水平方向重复，为重复值 

    repeat-x：仅在水平方向重复 

    repeat-y：仅在垂直方向重复 

    no-repeat：仅显示一次

- background-position 设置背景图像的起始位置

    ```css
    background-position:50px 100px;
    /* x 轴方向，相对于原来，向右平移50px ，y向下平移 100px */
    ```

- background-attachment 背景图像是否固定或者随着页面的其余部分滚动 

    默认值是 scroll：默认情况下，背景会随文档滚动 

    可取值为 fixed：背景图像固定，并不会随着页面的其余部分滚动，常用于实现称为水印的图像

*<u>CSS3新增</u>*

- background-origin：指定了背景图像的位置区域

    取值：border-box、padding-box、context-box

     ![image-20201225162227381](img/_02_CSS/image-20201225162227381.png)

- background-clip: 背景剪切

    取值：border-box、padding-box、context-box

     ![image-20201225162023015](img/_02_CSS/image-20201225162023015.png)

- background-size: 

    ```css
    background-size: contain;	/* 等比例缩放图像，使图像完整显示，比例不一致时可留空白 */ 
    background-size: cover;		/* 等比例缩放图像，尽可能的完全显示图像，但必须填满元素，比例不一致时，可剪切图片 */
    background-size: 100%;      /* 作用同contain，这里的百分数，是指图片占元素的百分之多少 */
    background-size: 100% 100%; /* 拉伸图像，100% 表示填满元素。 */
    ```

     ![image-20201225163758514](img/_02_CSS/image-20201225163758514.png)

- 渐变背景：

    ```css
    bcakground: linear-gradient(to left top, #330066, #558844, #885544);
    /* 线性渐变 参数为方向加颜色，方向可以为 to XXX 或者 30deg【30度角】 也可以省略，默认是从上到下；颜色可以有多个*/
    background: radial-gradient(#33425, #fd5ab4, #55cc6a);
    /* 参数为颜色列表，径向渐变，丛中心点向外渐变 */
    ```

## 7. 列表属性

```list-style-type 属性```

无序列表：可取值：**none**、disc【实心圆、默认】、square【方框】、circle【空心圆】

有序列表：可取值：**none**、decimal【数字、默认】、**decimal-leading-zero**【前边补0的数字】、lower-roman、upper-roman【罗马数字大小写】、lower-alpha、upper-alpha【字母大小写】

```list-style-image 属性```

取值为：url("图片路径")     ol  ul  皆可用。

若 type 和 image 都有指定，则 image 生效。

## 8. 边框及轮廓

**边框属性：**

border-width: 20px; 

border-style: solid;     取值可以为：solid【实线】 dash【虚线】 dotted【点】 double【双线】

border-color: green; 

*<u>css3属性</u>*

border-radius：10px； 大小超过短边的一半，再大也不会变了，设置为50%可以变成圆形或椭圆

border-shadow：5px 5px 3px 1px gray;  参数分别为 水平偏移量，竖直偏移量，模糊半径，扩散半径，阴影颜色。

**轮廓：**

轮廓（outline）是绘制于元素周围的一条线，位于边框边缘的外围，可起到突出元素的作用。

outline： 取值与border相同。

输入框获得焦点后，出现的黑色框框就是outline，常设置为none以去除此效果。

## 9. 盒子模型

​		所有HTML元素可以看作盒子，在CSS中，"box model"这一术语是用来设计和布局时使用。 CSS盒子模型本质上是一个盒子，封装周围的HTML元素，它包括：边距，边框，填充，和实际内容。 盒子模型允许我们在其它元素和周围元素边框之间的空间放置元素。

margin(外边距) - 盒子与盒子之间的距离 

border(边框) - 盒子的保护壳 

padding(内边距/填充) - 内填充，盒子边与内容之间的距离 

content(内容) - 盒子的内容，显示的文本或图像

![image-20201225112135489](img/_02_CSS/image-20201225112135489.png)

width与height指定的只是content的宽和高。所以，指定border，padding等都会将元素撑到，不要以为指定了宽高元素占的地儿就固定了。

backgroung的范围是包括border在内的所有东西。

## 10. CSS定位

### 10.1 默认排布

-   块级元素：h1~h6，p, div 等，自上而下，垂直排列（自动换行）；可以改变宽高
-   行内元素：a,b,span,等，从左向右，水平排列（不会换行）；不能改变宽高
-   行内块元素：input,img等，从左向右，水平排列（不会换行）；可以改变宽高

### 10.2 浮动

```float 属性``` 取值：left，right，none【默认】

作用：设置元素向左或向右浮动，浮动之后的元素会脱离原文档流，原文档流中元素会当它不存在，故而其后元素上移，其父元素高度塌陷。

浮动获得元素 Y 坐标 不会变，只是在左右方向上浮动，浮动之后的元素都为行内块级元素。元素浮动不会对其内部元素产生影响，其内部又是一个新的文档流。

>   文档流：
>
>   ​		指的是元素排版布局过程中，元素会自动从左往右，从上往下的流式排列。并最终窗体自上而下分成一行行，并在每行中按从左到右的顺序排放元素。脱离文档流即是元素打乱了这个排列，或是从排版中拿走。让元素脱离文档流的方法有：浮动和定位。

>   高度塌陷：
>
>   ​		当父元素未指定高度时，其高度由子元素计算，由于浮动的子元素已脱离源文档流，计算时其高度也自然会被忽略。从而造成子元素超出父元素的情况。

解决高度塌陷：

- 消除浮动

    在浮动的元素之后，添加一个空元素，指定空元素的clear【css属性】为both。

- ```overflow属性```

    设置父元素的overflow属性为 auto，或者 hidden

    注意：由于父元素未指定大小，这里的hidden并不会将超出的浮动元素影藏，而是将父元素变大以包容所有子元素。

### 10.3 定位

```position属性``` 可取值：absolute【绝对定位】、relative【相对定位】、fixed【固定定位】

1. 相对定位

    ```css
    position: relative;top: 20px;left: 40px;
    ```

    元素相对于其原来的位置进行偏移，top表示向下偏移多少，left表示向右偏移多少，取值可为负数。

2. 绝对定位

    本元素与**已定位**的祖先元素的距离

    ```css
    .parent{
        position: relative;
    }
    .child{
        position: absolute;
        top: 50px;
        left: 50px;
        bottom: 50px;
        right: 50px;
    }
    ```

    top left可以指定元素相对于，已定位祖先元素的位置。

    ​	注意：条件是已定位，即只要出现position即可，不一定要求其值非要是absolute。

    ​	绝对定位的元素会**从原文档流脱离**，故它无法撑起其父元素。

    可以同时指定 top、left、right、bottom来指定，元素与已定位父元素各边的间隔，从而确定子元素的大小。

    ​	注意这样的话，就要关注父元素的大小了，向上面这种，如果父元素中没有其他内容，而子元素又撑不起父元素，父元素大小就为0，从而子元素大小也就是0了，这时如果再指定child的overflow为hidden，那么就什么都显示不出来了。

3. 固定定位

    position设置为fixed，表示固定定位，该方式与的绝对定位相似，不过参照不再是已定位祖先元素，而是窗口。

    作用：常用作广告区域定位。

```z-index属性``` 

如果有重叠元素，使用z轴属性，定义上下层次。

注意： 

-   z轴属性，要配合相对或绝对定位来使用。 
-   z值没有额定数值（整型就可以，具体用数字几，悉听尊便）

## 11. 过渡

**过渡**：从一个状态到另一个状态，中间的“缓慢”过程；

缺点是，控制不了中间某个时间点。 t

transition｛1 2 3 4｝

1：过渡或动画模拟的css属性 

2：完成过渡所使用的时间（2s内完成） 

3：过渡函数。。。 linear【匀速】、ease【先快后慢】。。。

4：过渡开始出现的  延迟时间

示例：

```css
transition: width 2s linear 1s;
```

当宽度发生变化时，要先延时 1S ，然后在接下来的两秒内宽度均匀的逐渐变化。

目前，css3只开发出部分的过渡属性：

 ![image-20201225164702147](https://gitee.com/mochen2020/picture-warehouse/raw/master/img/image-20201225164702147.png)

## 12. 动画

从一个状态到另一个状态，过程中每个时间点都可以控制。

关键帧：@keyframes 动画帧 { from{} to{} } 或者{ 0%{} 20%{}... }

动画属性：animation{ 1 , 2 , 3 , 4 , 5 }

1：动画帧 2：执行时间 3：过渡函数 4：动画执行的延迟（可省略） 5：动画执行的次数

示例：

```css
.pp{
	animation: x 3s linear infinite alternate;  /* infinite：无限次、 alternate：来回执行（交替，一去一回） */
}
@keyframes x{
    0%{ margin-left: 0px; }
    25%{ background: yellowgreen; }
    50%{ background: goldenrod; }
    75%{ background: palevioletred;}
    100%{
        background: coral;
        margin-left: 550px;
    }
}
```