# HTML中常用标签易忘知识汇总

## 1. \<hr /\> 标签

作用：水平分割线

支持的属性：align   color  width(宽度)  size(粗细)

说明：\<hr/\>就是画一个水平分割线，所需样式不多，没必要通过css设置，所以要记一下它支持那些属性，以便快捷的设置它的样式。

## 2. 列表标签

```html
<ol type="A" start="5">
    <li>第一项</li>
    <li>第二项</li>
</ol>
<ul type="square">
    <li>第一项</li>
    <li>第二项</li>
</ul>
```

注意：

- \<ol\>中 type 可以 为 1-(数字)（默认）      A a-(字母)     I i-(罗马数字)

    start 的 值为数字， 表示起始值，如上例编号就是从  E 开始。

- \<ul\>中 type 可以 square-方框     cricle-空心圆   disc-实心圆（默认）

## 3. \<a\> 标签

```html
<a href="http://www.baidu.com" target="_blank">百度搜索</a>
```

**target属性的取值**：

1.   _blank ：在新浏览器窗口中打开链接文件
2.   _parent ：将链接的文件载入含有该链接框架的父框架集或父窗口中。如果含有该链接的框架不是嵌套的，则在浏览器全屏窗口中载入链接的文件，就象 _self 参数一样。
3.   _self ：在同一框架或窗口中打开所链接的文档。【默认值】
4.   _top ：在当前的整个浏览器窗口中打开所链接的文档，因而会删除所有框架。



**href属性的值**：

1. 目标网址

    注意：访问其他网站内容，要写上协议，如```http://``` ```https://```

2. js代码

    ```javascript
    <a href="javascript:void(0);" >点此无反应javascript:void(0)</a>
    <a href="javascript:;" >点此无反应javascript:</a>
    ```

    href属性还可以通过javascript计算获得，如上两种方式计算的返回值都是空，这样就实现了 a标签点击不跳转。

    实现a标签点击不跳转还可以使用如下方式：

    ```html
    <a href="" onclick="return false;">return false;</a>
    ```

    原理：onXXX 中 return false 后默认的事件响应将被取消。

3. 发邮件

    ```html
    <a href="mailto:136522@163.com">发邮件</a>
    ```

4. 发送QQ消息

    ```html
    <a href="tencent://message/?uin=2604472717&Menu=yes">发送QQ消息</a>
    ```



**name属性**

```html
<!-- 定义锚点 -->
<a name="red"></a>
<!-- 跳转到指定锚点 -->
<a href="#red"></a>
```

name可以设置a标签为一个锚点。而后可以通过href中 #xxx 的方式跳转。

如上是html4中锚点的使用方式，HTML5中任意元素的 id属性 都可以作为锚点使用。

## 4. \<table\>表格标签

**1. \<table\>：**定义一个表格 

- border属性：

    边框粗细，取值是像素为单位。默认为 0 ，无边框 。

    但取值不为0时，指定的也只是表格最外边的边框

- width 代表的表格总的宽度，然后每一列的宽度默认情况下，跟内容有关。

- align 代表 表格在页面中 的对齐方式；取值 left、 right、center

- cellspacing：单元格间距

    通常设置0表示**单线表格**，然而这是由线宽是border的两倍，也就是说还是两根线。

**2. \<tr\>：**表格中的行 （Table Row）

- align属性：统一设置这一行中，单元格内容的对齐方式；取值 left、 right、center

    但然也可以通过 td 的 align 属性单独指定某一单元格的对齐方式。

**3. \<td\>：**表格中的数据单元格 （Table DataCell） 

-   colspan属性： 指示列的合并
-   rowspan属性： 指示行的合并

**4. \<th\>** 定义表头，表头字体会被加粗，用法同 \<td\>



## 5. \<form\>表单

**1. \<form\>**

-   action属性：整个表单提交的目的地 
-   method：表单提交的方式 get 或 post

**2. 表单元素\<input\>**

type属性:

-   text：默认值，普通的文本输入框 placeholder属性：提示文本 maxlength属性：最多能输入字符数量 
-   password：密码输入框 
-   checkbox：多选框/复选框 checked：被选中 
-   radio：单选按钮 
-   file：上传文件 
-   reset：重置按钮 
-   submit：提交按钮 
-   button：普通按钮 ：

h5新增type属性：

-   color：调色板 
-   date：日历    month：月历    week：周历 
-   number：数值域 
    -   min：最小值（默认值是1） 
    -   max：最大值（默认值无上限） 
    -   step：递增量 
-   range：滑块 
-   search：搜索框（**带×号**，可一键删除框中内容）

**其他属性：**

-   placeholder  指定内容为空时的提示信息
-   require 表示必填项
-   maxlength 指定 最多 多少个字符

**2. 表单元素\<select name="\*\*\*"\>**

​	子元素  \<option value="XXX"> XXX   

​	selected="selected"   表示默认被选中 

**3. 表单元素 \<textarea\>**

文本域标签（多行文本框） 可以通过 cols 和 rows 属性来规定 textarea 的尺寸，不过更好的办法是使用 CSS 的height 和 width 属性。

**4. 表单元素 \<button\>：** 

在form表单中，作用和submit一样 

不在form表单中，就是普通按钮

**5. H5新增表单元素**

- 进度条\<progress\>

- 高亮\<mark\>  就是加个底色 突出显示

- 联想输入框 \<datalist\>（模糊查询）

    举例：

    ```html
    <input type="text" list="xxxName">
    <datalist>
    	<option>123</option>
    	<option>456</option>
    	<option>789</option>
    </datalist>
    ```

    这样在input标签中输入时，就会根据现有输入在datalist列表中进行模糊匹配，并将结果列举出来以供选择。

- \<label\> 标签

    ```html
    <!-- 方式一：通过id绑定 -->
    <label for="xxxName">用户名：</label>
    <input type="text" id="xxxName">
    <!-- 方式二：通过嵌套绑定 -->
    <label>密码：<input type="password" /></label>
    
    <!-- 
    label的作用：
    	如上，就可以通过点击，“用户名” 或 “密码”，聚焦到对应的输入框	
    -->
    ```

**注意事项：** 

1.  所有表单中的元素都要具有名称（否则提交到服务器之后，服务器无法区识别多个元素之间的不 同） 
2.  单选框要想可以一次只选择一个，要具有相同的name值 
3.  所有的复选框以组为单位，组内的每个复选框都应该具有相同的name值

## 6. \<frame\>框架

```html
<frameset rows="10%,*,13%">
    <frame name="top" src="top.html"></frame>
    <frameset cols="15%,*">
        <frame name="left" src="left.html"></frame>
    	<frame name="right" src="right.html"></frame>
    </frameset>
    <frame name="foot" src="foot.html"></frame>
</frameset>
```

注意：frameset与body只能有一个。

## 7. \<meta>标签

```html
<!-- 设置页面字符集 -->
<meta charset="UTF-8">
<!-- 设置显示窗口宽度为屏幕宽度，针对于移动设备 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  每个电脑内置的IE版本是不一样的，为了兼容所有的版本以最高级模式渲染文档，也就是任何IE版本都以当前版本所支持的最高级标准模式渲染-->
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!-- 5秒后，跳转到指定页面 -->
<meta http-equiv="refresh" content="5; url=http://www.lagou.com">
```

## 8. 特殊字符

![image-20201223173642894](img/_01_html标签/image-20201223173642894.png)

![image-20201223173727816](img/_01_html标签/image-20201223173727816.png)

## 9. 语义化标签

语义化标签，功能上与div一样，好处是 见名知意，使代码结构清晰。

-   section标签：表示页面中的内容区域，部分，页面的主体部分 
-   article标签：文章 
-   aside标签：文章内容之外的，标题 
-   **header标签：头部，页眉，页面的顶部** 
-   hgroup标签：内容与标题的组合 
-   **nav标签：导航** 
-   figure标签：图文并茂 
-   **foot：页脚，页面的底部**

## 10 \<video\>标签

属性：

-   src：媒体资源文件的位置 
-   controls：控制面板
-   autoplay：自动播放（谷歌失效，360浏览器可以） 
-   loop：循环播放



















