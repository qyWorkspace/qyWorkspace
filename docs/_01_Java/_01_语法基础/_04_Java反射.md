# 反射

反射是指：在运行阶段决定创建什么对象，调用什么方法的编程机制

## 1、Class类

1. 基本概念

    -   java.lang.Class类的实例可以描述Java的类和接口，也就是一种数据类型。
    -   它没有公共构造方法，其实例由 JVM 和 ClassLoader 自动创建。

2. 获取方式

    ```java
    // 1. 数据类型 * 
    String.class();    // class java.lang.String
    int.class();	   // int
    void.class();	   // void
    
    // 2. 对象.getClass()
    obj.getClass();
    
    // 3. 包装类.type()  // 获取其基本类型的 Class 对象
    Integer.type;    // int
    Integer.class;   // java.lang.Integer
    
    // 4. forName方法 *
    Class.forName("完全限定名");
    
    // 5. 类加载器
    ClassLoader cl = A.class.getClassLoader();
    cl.loadClass("完全限定名")
    ```

3. 常用方法

    ```java
    newInstance();   // 调用无参构造，创建对象，已过时Constructor<T> getConstructor(Class<?>... parameterTypes); // 获取Class对象代表的类型中对应参数的公共构造方法。  // 注意这里参数 必须是Class的对象Constructor<T>[] getConstructors();  // 获取 所代表的 类型的所有公 共构造器Field getDeclaredField(String name);  // 获取 字段对象，Declared表示获取所有的，不加只能获取public的。Field[] getDeclareFields();			  // 获取所有字段Method getMethod(String name,Class<?>... parameterTypes);// 用于获取该Class对象表示类中名字为name参数为parameterTypes的指定公共成员方法Method[] getMethods(); // 用于获取该Class对象表示类中所有公共成员方法
    ```

## 2、Constructor类

1. 获取实例

    通过Class对象的getConstructor方法获取

2. 常用方法

    ```java
    T newInstance(Object... initargs);  // 创建实例 **int getModifiers();                 // 获取方法的访问修饰符，【因为不只可以获取到共有构造方法】String getName();					// 获取方法名，不知有何用？Class<?>[] getParameterTypes();     // 获取方法的所有参数类型
    ```

3. 示例

    ```java
    public static void main(String[] args) throws Exception{    Class pclass = Class.forName("mc.PC.Person");    Constructor constructor = pclass.getConstructor(String.class,int.class);    Object o = constructor.newInstance("Jsumy",15);    System.out.println(o);}
    ```

    

## 3、Feild类

常用方法：

```java
Object get(Object obj);  // 获取字段的值，obj为要从那个对象获取。void set(Object obj,Object value);   // 设置字段值void setAccessible(boolean flag); //当传true时，反射对象在使用时取消 Java 语言访问检查【可访问私有】int getModifiers();  // 获取成员变量的访问修饰符Class<?> getType();  // 获取成员变量的数据类型String getName(); 	 // 获取成员变量的名称
```

## 4、Method类

```java
Object invoke(Object obj,Object... args); // 使用对象obj来调用此Method对象所表示的成员方法，实参传递
argsint getModifiers(); // 获取方法的访问修饰符
Class<?> getReturnType(); // 获取方法的返回值类型
String getName(); // 获取方法的名称
Class<?>[] getParameterTypes(); // 获取方法所有参数的类型
Class<?>[] getExceptionTypes(); // 获取方法的异常信息
```

## 5、其他信息

```java
Package getPackage(); // 获取所在的包信息
Class<? super T> getSuperclass(); // 获取继承的父类信息
Class<?>[] getInterfaces(); // 获取实现的所有接口
Annotation[] getAnnotations(); // 获取注解信息
Type[] getGenericInterfaces(); // 获取泛型信息
```














