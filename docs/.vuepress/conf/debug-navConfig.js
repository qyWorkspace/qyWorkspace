// 此文件 [nav配置] 进用于debug，没有其他作用。
module.exports = [
    {
        "text": "首页",
        "link": "/"
    },
    {
        "text": "Java",
        "items": [
            {
                "text": "语法基础",
                "link": "/_01_Java/_01_语法基础/_04_Java反射"
            },
            {
                "text": "常用工具类",
                "link": "/_01_Java/_02_常用工具类/_01_OkHttp3"
            }
        ]
    },
    {
        "text": "前端",
        "items": [
            {
                "text": "HTML&CSS",
                "link": "/_02_前端/_01_HTML&CSS/_01_html标签"
            },
            {
                "text": "node.js",
                "link": "/_02_前端/_03_node.js/_01_nodejs"
            },
            {
                "text": "Vue",
                "link": "/_02_前端/_04_Vue/vueCliService"
            }
        ]
    },
    {
        "text": "数据库",
        "items": [
            {
                "text": "readme",
                "link": "/_03_数据库/"
            }
        ]
    },
    {
        "text": "软件",
        "items": [
            {
                "text": "readme",
                "link": "/_04_软件/"
            }
        ]
    },
    {
        "text": "操作系统",
        "items": [
            {
                "text": "readme",
                "link": "/_05_操作系统/"
            }
        ]
    },
    {
        "text": "算法",
        "items": [
            {
                "text": "readme",
                "link": "/_06_算法/"
            }
        ]
    },
    {
        "text": "开发工具",
        "items": [
            {
                "text": "Maven",
                "link": "/_07_开发工具/_01_Maven/Maven多环境配置"
            }
        ]
    },
    {
        "text": "源码与经验",
        "items": [
            {
                "text": "commons-pool2",
                "link": "/_08_源码与经验/_01_commons-pool2/_01_基本使用"
            }
        ]
    }
]