// 此文件 [sidebar配置] 进用于debug，没有其他作用。
module.exports = {
    "/_01_Java/_01_语法基础/": [
        {
            "title": "语法基础",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "_04_Java反射",
                    "Java反射"
                ],
                [
                    "_05_常用操作",
                    "常用操作"
                ]
            ]
        }
    ],
    "/_01_Java/_02_常用工具类/": [
        {
            "title": "常用工具类",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "_01_OkHttp3",
                    "OkHttp3"
                ],
                [
                    "_02_加解密",
                    "加解密"
                ]
            ]
        }
    ],
    "/_02_前端/_01_HTML&CSS/": [
        {
            "title": "HTML&CSS",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "_01_html标签",
                    "html标签"
                ],
                [
                    "_02_CSS",
                    "CSS"
                ]
            ]
        }
    ],
    "/_02_前端/_03_node.js/": [
        {
            "title": "node.js",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "_01_nodejs",
                    "nodejs"
                ]
            ]
        }
    ],
    "/_02_前端/_04_Vue/": [
        {
            "title": "Vue",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "vueCliService",
                    "vueCliService"
                ]
            ]
        }
    ],
    "/_07_开发工具/_01_Maven/": [
        {
            "title": "Maven",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "Maven多环境配置",
                    "Maven多环境配置"
                ],
                [
                    "Maven打包",
                    "Maven打包"
                ]
            ]
        }
    ],
    "/_08_源码与经验/_01_commons-pool2/": [
        {
            "title": "commons-pool2",
            "collapsable": false,
            "sidebarDepth": 1,
            "children": [
                [
                    "_01_基本使用",
                    "基本使用"
                ]
            ]
        }
    ]
}