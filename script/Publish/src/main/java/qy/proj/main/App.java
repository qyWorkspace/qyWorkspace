package qy.proj.main;

import qy.proj.module.AutoDeploy;
import qy.proj.module.CmdUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class App {

    public static void main(String[] args) throws Exception {
        // 博文提交
        CmdUtils.exec("git status");
        CmdUtils.exec("git add .");
        String commitStr = new SimpleDateFormat("yyyy年MM月dd日-YY:mm:ss 提交笔记内容。").format(new Date());
        CmdUtils.exec("git commit -m \"" + commitStr + "\"");

        // 编译
        CmdUtils.exec("cmd.exe /C npm install");
        CmdUtils.exec("cmd.exe /C npm run build");

        // 提交编译结果
        CmdUtils.exec("git add .");
        commitStr = new SimpleDateFormat("yyyy年MM月dd日-YY:mm:ss 提交编译结果。").format(new Date());
        CmdUtils.exec("git commit -m \"" + commitStr + "\"");

        // 提交 推送到 gitee
        CmdUtils.exec("git push origin master");

        // gitee 部署发布
        boolean deploy = AutoDeploy.deploy();
        if(!deploy){
            // 若失败了就再试一次
            AutoDeploy.deploy();
        }
    }

}
