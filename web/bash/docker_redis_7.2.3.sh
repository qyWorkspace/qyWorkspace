#!/bin/bash

# ============================= 说明 =============================
# 执行此脚本用于在安装有 docker 和 docker-compose 的机器上安装 redis:7.2.3
#
# 执行脚本需要输入：
#   - 无
#
# 执行完成后会在执行脚本的目录中生成 redis 文件夹
# - redis
#     - conf 此目录用于存放 redis 的配置文件，文件名只能是 redis.conf
#            目录下还会包含 redis.conf.default 该文件是原始配置文件的备份，以备将来查看
# 执行完脚本后 redis 容器会自行启动，进入 redis 目录后可使用 docker-compose 管理此容器
#
# ===============================================================


# 程序入口
function main(){
    # 当前所在目录
    cur_path=$(pwd)
    # 指定安装 redis 的文件夹名字
    install_dir_name=redis


    if [ -d "./$install_dir_name" ]; then
        echo $cur_path/$install_dir_name $(error 已存在)，是否已安装完毕? 重装请删除该目录或切换到其他目录
        exit 0
    else
        mkdir -p $cur_path/$install_dir_name/conf

        # 添加自定义的 redis 配置文件
        curl -o $cur_path/$install_dir_name/conf/redis.conf https://qyworkspace.gitee.io/bash/redis.conf
        # 添加默认配置的备份
        curl -o $cur_path/$install_dir_name/conf/redis.conf.default https://qyworkspace.gitee.io/bash/redis.conf

        # 添加 docker-compose.yml 文件
        print_docker_compose_file $cur_path/$install_dir_name/docker-compose.yml $password

        # 检查 web_base_net 网络是否存在
        check_base_net

        # 启动容器
        cd ./$install_dir_name
        docker-compose up -d

    fi
}

# 检查 web_base_net 网络是否存在
function check_base_net(){
    # 检查网络是否存在
    if [[ ! "$(docker network ls -q -f name=web_base_net)" ]]; then
        # 所需网络不存在
        echo -n "正在创建网络 'web_base_net' ... "
        docker network create web_base_net
        echo $(success [成功])
    else
        echo "网络 'web_base_net' 已存在"
    fi
}

# 输出 docker-compose.yml 文件
# $1: 输出的文件位置
function print_docker_compose_file(){
    # 当前所在目录
    cur_path=$(pwd)
    cat << EOF > $1
version: '3.1'

services:
  redis:
    image: redis:7.2.3
    container_name: redis
    volumes:
      - "$cur_path/redis/conf:/usr/local/etc/redis"
    command:
      - redis-server
      - /usr/local/etc/redis/redis.conf
    ports:
      - "6379:6379"
    networks:
      - web_base_net
    restart: always

networks:
    web_base_net:
        driver: bridge
        external: true

EOF
}

# 输出红色文本
error() {
    echo $(change_text_color "31" $1)
}
# 输出绿色文本
success() {
    echo $(change_text_color "32" $1)
}
# 输出黄色文本
warn() {
    echo $(change_text_color "33" $1)
}
# 输出蓝色色文本
primary() {
    echo $(change_text_color "34" $1)
}
# 输出不同颜色的文本
change_text_color() {
    echo -e "\033[0;$1m$2\033[0m"
}

main