#!/bin/bash

# ============================= 说明 =============================
# 执行此脚本用于在 centos7 机器上安装 jdk17.0.1
#
# 执行脚本需要输入：
#   - 无
# ===============================================================

# 检查命令是否存在函数
check_command() {
    command -v $1 >/dev/null 2>&1 || { echo >&2 "$1 未安装，请先安装 $1 或者将其加入系统 PATH 路径"; exit 1; }
}

# 检查必要命令是否存在
check_command wget
check_command tar

# 设置要安装的JDK版本和下载链接
jdk_version="17.0.1"
download_link="https://d6.injdk.cn/openjdk/openjdk/17/openjdk-17.0.1_linux-x64_bin.tar.gz"

# 设置安装目录
install_directory="/opt/jdk$jdk_version"

# 创建安装目录
sudo mkdir -p "$install_directory"

# 下载JDK压缩包到安装目录
echo "正在下载 JDK $jdk_version 到安装目录..."
sudo wget --progress=bar "$download_link" -O "$install_directory/jdk.tar.gz"

# 解压缩到安装目录
if [ -e "$install_directory/jdk.tar.gz" ]; then
    echo "解压缩 JDK 到 $install_directory..."
    sudo tar -xzf "$install_directory/jdk.tar.gz" -C "$install_directory" --strip-components=1
    sudo rm "$install_directory/jdk.tar.gz"  # 删除临时下载的压缩包

    # 配置JDK环境变量
    echo "设置 JDK $jdk_version 环境变量..."
    echo "export JAVA_HOME=$install_directory" | sudo tee -a /etc/profile >/dev/null
    echo 'export PATH=$PATH:$JAVA_HOME/bin' | sudo tee -a /etc/profile >/dev/null
    sudo update-alternatives --install "/usr/bin/java" "java" "$install_directory/bin/java" 1
    sudo update-alternatives --install "/usr/bin/javac" "javac" "$install_directory/bin/javac" 1
    sudo update-alternatives --set java "$install_directory/bin/java"
    sudo update-alternatives --set javac "$install_directory/bin/javac"
    source /etc/profile
    echo "JDK $jdk_version 环境变量已设置"
else
    echo "下载JDK失败，请检查链接或网络连接"
fi
