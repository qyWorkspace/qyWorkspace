#!/bin/bash

# ============================= 说明 =============================
# 执行此脚本用于设置docker开放远程连接。
#
# 执行脚本需要输入：
#   - 主机 IP （通过 ifconfig 命令查看）
#   - 密码（随机输入一个密码即可，后续使用也用不到）
#
# 执行完成后会开放docker远程访问，并在 /etc/docker 目录下生成秘钥文件，
# 将之下载到需要远程连接的电脑上使用即可。
# ===============================================================


#相关配置信息
#docker主机IP
read -p "请输入Docker主机IP：" SERVER
echo $SERVER
# 检查SERVER是否为空
if [ -z "$SERVER" ]; then
    echo "Docker主机IP不能为空！"
    exit 1  # 退出脚本
fi
read -p "请输入密码：" PASSWORD
# 检查SERVER是否为空
if [ -z "$PASSWORD" ]; then
    echo "密码不能为空！"
    exit 1  # 退出脚本
fi
COUNTRY="CN"
STATE="zhejiang"
CITY="hangzhou"
ORGANIZATION="zoleet"
ORGANIZATIONAL_UNIT="Dev"
EMAIL="123456789@qq.com"

###开始生成文件###
echo "开始生成文件"

#切换到生产**的目录
cd /etc/docker
#生成ca私钥(使用aes256加密)
openssl genrsa -aes256 -passout pass:$PASSWORD  -out ca-key.pem 2048
#生成ca证书，填写配置信息
openssl req -new -x509 -passin "pass:$PASSWORD" -days 3650 -key ca-key.pem -sha256 -out ca.pem -subj "/C=$COUNTRY/ST=$STATE/L=$CITY/O=$ORGANIZATION/OU=$ORGANIZATIONAL_UNIT/CN=$SERVER/emailAddress=$EMAIL"

#生成server证书私钥文件
openssl genrsa -out server-key.pem 2048
#生成server证书请求文件
openssl req -subj "/CN=$SERVER" -new -key server-key.pem -out server.csr

echo "subjectAltName=IP:${SERVER},IP:0.0.0.0" >> extfile.cnf
echo "extendedKeyUsage=serverAuth" >> extfile.cnf

#使用CA证书及CA**以及上面的server证书请求文件进行签发，生成server自签证书
openssl x509 -req -days 3650 -in server.csr -CA ca.pem -CAkey ca-key.pem -passin "pass:$PASSWORD" -CAcreateserial  -out server-cert.pem -extfile extfile.cnf

#生成client证书RSA私钥文件
openssl genrsa -out key.pem 2048
#生成client证书请求文件
openssl req -subj '/CN=client' -new -key key.pem -out client.csr

sh -c 'echo "extendedKeyUsage=clientAuth" > extfile.cnf'

#生成client自签证书（根据上面的client私钥文件、client证书请求文件生成）
openssl x509 -req -days 3650 -in client.csr -CA ca.pem -CAkey ca-key.pem  -passin "pass:$PASSWORD" -CAcreateserial -out cert.pem  -extfile extfile.cnf

#更改**权限
chmod 0400 ca-key.pem key.pem server-key.pem
#更改**权限
chmod 0444 ca.pem server-cert.pem cert.pem
#删除无用文件
rm client.csr server.csr

echo "生成文件完成"
###生成结束###

echo "开始修改/usr/lib/systemd/system/docker.service"
LINES=`sed -n -e '/ExecStart/=' /usr/lib/systemd/system/docker.service`
LASEST_LINE_NUM=0
for LINE_NUM in $LINES
do
  sed "${LINE_NUM}s/^/# &/g" -i /usr/lib/systemd/system/docker.service
  LASEST_LINE_NUM=$LINE_NUM
done
sed "${LASEST_LINE_NUM}i ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --tlsverify --tlscacert=/etc/docker/ca.pem --tlscert=/etc/docker/server-cert.pem --tlskey=/etc/docker/server-key.pem -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock" -i /usr/lib/systemd/system/docker.service
echo "修改服务配置文件完成"

echo ""
# 重新加载文件
systemctl daemon-reload
systemctl restart docker.service

# 开放防火墙端口
firewall-cmd --zone=public --add-port=2375/tcp --permanent
firewall-cmd --reload

