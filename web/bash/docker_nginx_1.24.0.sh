#!/bin/bash

# ============================= 说明 =============================
# 执行此脚本用于在安装有 docker 和 docker-compose 的机器上安装 Nginx:1.24.0
#
# 执行脚本需要输入：
#   - 无
#
# 执行完成后会在执行脚本的目录中生成 nginx 文件夹
# - nginx
#     - html 此目录用于存放 nginx 的静态资源，默认会生成一个 index.html
#     - conf 此目录用于存放 nginx 的配置文件，
#            此脚本会自动生成一个默认配置文件用于配置静态资源所在目录
# 执行完脚本后 nginx 容器会自行启动，进入 nginx 目录后可使用 docker-compose 管理此容器
#
# ===============================================================


# 程序入口
function main(){
    # 当前所在目录
    cur_path=$(pwd)
    # 指定安装 nginx 的文件夹名字
    install_dir_name=nginx


    if [ -d "./$install_dir_name" ]; then
        echo $cur_path/$install_dir_name $(error 已存在)，是否已安装完毕? 重装请删除该目录或切换到其他目录
        exit 0
    else
        mkdir -p $cur_path/$install_dir_name/html
        mkdir -p $cur_path/$install_dir_name/conf

        # 添加自定义的 nginx 配置文件
        print_config_file $cur_path/$install_dir_name/conf/nginx.conf

        # 添加自定义的 index.html
        print_index_file $cur_path/$install_dir_name/html/index.html

        # 添加 docker-compose.yml 文件
        print_docker_compose_file $cur_path/$install_dir_name/docker-compose.yml $password

        # 检查 web_base_net 网络是否存在
        check_base_net

        # 启动容器
        cd ./$install_dir_name
        docker-compose up -d

    fi
}

# 检查 web_base_net 网络是否存在
function check_base_net(){
    # 检查网络是否存在
    if [[ ! "$(docker network ls -q -f name=web_base_net)" ]]; then
        # 所需网络不存在
        echo -n "正在创建网络 'web_base_net' ... "
        docker network create web_base_net
        echo $(success [成功])
    else
        echo "网络 'web_base_net' 已存在"
    fi
}

# 输出 docker-compose.yml 文件
# $1: 输出的文件位置
function print_docker_compose_file(){
    # 当前所在目录
    cur_path=$(pwd)
    cat << EOF > $1
version: '3.1'

services:
    nginx:
        image: nginx:1.24.0
        container_name: nginx
        ports:
            - "80:80"
            - "443:443"
        volumes:
            - "$cur_path/nginx/html:/usr/share/nginx/html"
            - "$cur_path/nginx/conf:/etc/nginx/conf.d"
        networks:
            - web_base_net
        restart: always

networks:
    web_base_net:
        driver: bridge
        external: true

EOF
}

# 输出自定义 nginx 配置文件
# $1: 输出的文件位置
function print_config_file(){
    cat << EOF > $1
server {
    listen 80;

    location / {
        root /usr/share/nginx/html;
    }
}

EOF
}

# 输出自定义 index.html 文件
# $1: 输出的文件位置
function print_index_file(){
    cat << EOF > $1
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首页</title>
</head>
<body>
    <center style="color: blueviolet;margin-top: 120px;"><h1>NGINX，HELLO</h1></center>
    <center style="color: green;">您的 Nginx 已成功安装，并正常运行！</center>
</body>
</html>
EOF
}

# 输出红色文本
error() {
    echo $(change_text_color "31" $1)
}
# 输出绿色文本
success() {
    echo $(change_text_color "32" $1)
}
# 输出黄色文本
warn() {
    echo $(change_text_color "33" $1)
}
# 输出蓝色色文本
primary() {
    echo $(change_text_color "34" $1)
}
# 输出不同颜色的文本
change_text_color() {
    echo -e "\033[0;$1m$2\033[0m"
}

main