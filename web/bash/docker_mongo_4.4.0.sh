#!/bin/bash

# ============================= 说明 =============================
# 执行此脚本用于在安装有 docker 和 docker-compose 的机器上安装 mongo:4.4.0
#
# 执行脚本需要输入：
#   - 无
#
# 执行完成后会在执行脚本的目录中生成 mongo 文件夹
# - mongo
#   - data mongo数据表的存储目录

# 执行完脚本后 mongo 容器会自行启动，进入 mongo 目录后可使用 docker-compose 管理此容器
#
# ===============================================================


# 程序入口
function main(){
    # 当前所在目录
    cur_path=$(pwd)
    # 指定安装 mongo 的文件夹名字
    install_dir_name=mongo


    if [ -d "./$install_dir_name" ]; then
        echo $cur_path/$install_dir_name $(error 已存在)，是否已安装完毕? 重装请删除该目录或切换到其他目录
        exit 0
    else
        # 让用户输入密码（不回显）
        password=$(input_password MONGODB)
        if [ $? -ne 0 ]; then
            exit 0
        fi

        mkdir -p $cur_path/$install_dir_name/data

        # 添加 docker-compose.yml 文件
        print_docker_compose_file $cur_path/$install_dir_name/docker-compose.yml $password

        # 检查 web_base_net 网络是否存在
        check_base_net

        # 启动容器
        cd ./$install_dir_name
        docker-compose up -d

    fi
}

# 检查 web_base_net 网络是否存在
function check_base_net(){
    # 检查网络是否存在
    if [[ ! "$(docker network ls -q -f name=web_base_net)" ]]; then
        # 所需网络不存在
        echo -n "正在创建网络 'web_base_net' ... "
        docker network create web_base_net
        echo $(success [成功])
    else
        echo "网络 'web_base_net' 已存在"
    fi
}

# 输出 docker-compose.yml 文件
# $1: 输出的文件位置
function print_docker_compose_file(){
    # 当前所在目录
    cur_path=$(pwd)
    cat << EOF > $1
version: '3.1'

services:

  mongo:
    image: mongo:4.4.0
    container_name: mongo
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: $2
    volumes:
      - "$cur_path/mongo/data:/data/db"
    ports:
      - "27017:27017"
    networks:
      - web_base_net
    restart: always

networks:
    web_base_net:
        driver: bridge
        external: true

EOF
}

# 让用户输入密码
# $1 提示词
input_password() {
    read -s -p "请输入 $1 密码：" password
    echo > /dev/tty # 打印一个换行，使下一个输入提示在新的一行显示
    if [ -z "$password" ]; then
        echo "密码不能为空，请重新输入" > /dev/tty
        return 1
    fi

    read -s -p "请再次输入密码：" password_confirmation
    echo > /dev/tty # 打印一个换行，使下一个输入提示在新的一行显示
    if [ "$password" != "$password_confirmation" ]; then
        echo "两次输入的密码不一致，请重新输入" > /dev/tty
        return 1
    fi

    echo "$password"
}

# 输出红色文本
error() {
    echo $(change_text_color "31" $1)
}
# 输出绿色文本
success() {
    echo $(change_text_color "32" $1)
}
# 输出黄色文本
warn() {
    echo $(change_text_color "33" $1)
}
# 输出蓝色色文本
primary() {
    echo $(change_text_color "34" $1)
}
# 输出不同颜色的文本
change_text_color() {
    echo -e "\033[0;$1m$2\033[0m"
}

main